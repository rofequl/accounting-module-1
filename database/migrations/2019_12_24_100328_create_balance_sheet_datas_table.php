<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBalanceSheetDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('balance_sheet_datas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('income_source_id')->nullable();
            $table->string('account_name')->nullable();
            $table->string('amount');
            $table->integer('balance_sheet_id')->nullable();
            $table->boolean('account_type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('balance_sheet_datas');
    }
}
