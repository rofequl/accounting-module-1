<?php

namespace App\Http\Controllers\API;

use App\balance_sheet;
use App\balance_sheet_data;
use App\credit;
use App\debit;
use App\income_source;
use App\payment;
use DateTime;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class BalancesheetController extends Controller
{
    public function index()
    {
        $bs = balance_sheet::latest()->first();
        $bs ? $bs_no = $bs->id + 1 : $bs_no = 1;

        if ($bs) {
            $start_date = DateTime::createFromFormat('d/m/Y', $bs->end_date);
            $start_date->modify('+1 day');
            $start_date = $start_date->format('Y-m-d');
        } else {
            $debit = debit::select('date')->groupBy('date')->get()->toArray();
            $credit = credit::select('date')->groupBy('date')->get()->toArray();
            $data = array_merge($debit, $credit);
            $collectionOld = new Collection($data);
            $collectionOld = $collectionOld->sortBy('date')->first();
            $start_date = $collectionOld['date'];
        }

        $output = array(
            'bs_no' => $bs_no,
            'start_date' => $start_date,
        );
        return json_encode($output);
    }

    public function store(Request $request)
    {
        $start_date = DateTime::createFromFormat('d/m/Y', $request->start_date);
        $end_date = DateTime::createFromFormat('d/m/Y', $request->end_date);

        $credit = credit::where('date', '>=', $start_date->format('Y-m-d'))
            ->where('date', '<=', $end_date->format('Y-m-d'))->latest()->get();
        $debit = debit::where('date', '>=', $start_date->format('Y-m-d'))
            ->where('date', '<=', $end_date->format('Y-m-d'))->latest()->get();
        $profit = $credit->sum('amount') - $debit->sum('amount');

        $output = array(
            'profit' => $profit,
            'debit' => $debit->sum('amount'),
        );
        return json_encode($output);
    }

    public function IncomeSourceAmount(Request $request)
    {
        $start_date = DateTime::createFromFormat('d/m/Y', $request->start_date);
        $end_date = DateTime::createFromFormat('d/m/Y', $request->end_date);

        $credit = credit::where('date', '>=', $start_date->format('Y-m-d'))
            ->where('date', '<=', $end_date->format('Y-m-d'))->where('department_id', $request->department_id)
            ->where('income_source_id', $request->income_source_id)->first();
        if ($credit)
            return $credit->amount;
        else
            return 0;
    }

    public function BalancesheetStore(Request $request)
    {
        $bs = balance_sheet::latest()->first();
        $bs ? $bs_no = $bs->id + 1 : $bs_no = 1;
        if ($bs_no == $request->bs_no) {
            $payment = payment::all();
            $balance = new balance_sheet();
            $balance->start_date = $request->start_date;
            $balance->end_date = $request->end_date;
            $balance->amount = $payment->sum('amount');
            $balance->save();

            foreach ($payment as $payments) {
                $balance_data = new balance_sheet_data();
                $balance_data->account_name = $payments->name;
                $balance_data->amount = $payments->amount;
                $balance_data->balance_sheet_id = $balance->id;
                $balance_data->account_type = 1;
                $balance_data->save();
            }

            foreach ($payment as $payments) {
                $balance_data = new balance_sheet_data();
                $balance_data->account_name = $payments->name;
                $balance_data->amount = $payments->previous_amount;
                $balance_data->balance_sheet_id = $balance->id;
                $balance_data->account_type = 2;
                $balance_data->save();
            }

            foreach ($request->balance_sheets as $balance_sheets) {
                if ($balance_sheets['balance'] !== 0) {
                    $balance_data = new balance_sheet_data();
                    $balance_data->income_source_id = $balance_sheets['income_source_id'];
                    $balance_data->account_name = income_source::find($balance_sheets['income_source_id'])->income_source;
                    $balance_data->amount = $balance_sheets['balance'];
                    $balance_data->balance_sheet_id = $balance->id;
                    $balance_data->account_type = 2;
                    $balance_data->save();
                }
            }

            foreach ($payment as $payments) {
                $balance_data = payment::find($payments->id);
                $balance_data->previous_amount = $payments->amount;
                $balance_data->save();
            }

            return 'ok';
        } else {
            return 'fail';
        }

    }

    public function GetData()
    {
        return balance_sheet::all();
    }

    public function GetDataAll($id)
    {
        return balance_sheet_data::where('balance_sheet_id', $id)->get();
    }
}
